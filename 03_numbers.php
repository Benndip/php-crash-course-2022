<?php

// Declaring numbers

    $a = 6;
    $b = 7;
    $c = 8;
// Arithmetic operations
    
    echo ($a + $b) * $c . '<br>';
    echo ($a - $b) . '<br>';
    echo ($a + $b) . '<br>';
    echo ($a / $b) . '<br>';
    echo ($a % $b) . '<br>';
// Assignment with math operators

// Increment operator

    $number = $a++;
    $otherNumber = ++$a;
    echo $number . '<br>';
    echo $otherNumber . '<br>';
// Decrement operator

    $number = $a--;
    $otherNumber = --$a;
    echo $number . '<br>';
    echo $otherNumber . '<br>';
// Number checking functions

    is_float(1.25); // true
    is_double(1.25); //true
    is_numeric("4.5"); // true
    is_numeric("4y.5"); // false
// Conversion

    $strNumber = "12.42";
    $number = (float)$strNumber;
    var_dump($number);
// Number functions
    echo pow(2,4). '<br>';
// Formatting numbers
    $number = 1233524.5463;
    echo number_format($number, 2, '.', ',');
// https://www.php.net/manual/en/ref.math.php

<?php

// What is a variable

// Variable types

// Declare variables
$name = 'Zurah';
$age = 25;
$isMale = true;
$height = 1.65;
$salary = null;

// Print the variables. Explain what is concatenation

echo $name;

// Print types of the variables

echo gettype($name) . '<br/>';

// Print the whole variable

var_dump($name) . '<br/>';
// Change the value of the variable

// Print type of the variable

// Variable checking functions

// Check if variable is defined
isset($name);

// Constants
define('PI', 3.14);
// Using PHP built-in constants

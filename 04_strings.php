<?php

// Create simple string
    $name = 'Benndip';
    $string = 'Hello I am ' . $name . ' and I am 22';
    $string2 = "Hello my name is $name ";
    echo $string . '<br>';
    echo $string2 . '<br>';
// String concatenation

// String functions

// Multiline text and line breaks

    $longText = "
        Hello my name is Benndip,
        I am 22 and I love tech and music.
    ";

    echo nl2br($longText) . '<br>';

// Multiline text and reserve html tags

// https://www.php.net/manual/en/ref.strings.php